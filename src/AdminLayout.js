import React, { Component } from 'react';
import {
  Link,
  Redirect
} from 'react-router-dom';

class AdminLayout extends Component {
  render() {
    return (
      <div>
        <ul className="nav" style={{borderBottom: 'solid thin rgb(200,200,200)'}}>
          <li>
            <Link to="/admin/movies" className="nav-link">Filmy</Link>
          </li>
          <li>
            <Link to="/admin/cinemas" className="nav-link">Kiná</Link>
          </li>
          <li>
            <Link to="/admin/schedules" className="nav-link">Rozpis kín</Link>
          </li>
          <li>
            <Link to="/admin/reservations" className="nav-link">Rezervácie</Link>
          </li>
        </ul>
        {this.props.children}
      </div>
    );
  }
}

export default AdminLayout;
