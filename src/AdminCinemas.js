import React, { Component } from 'react';
import {
  Link,
  Redirect
} from 'react-router-dom';
import axios from 'axios';
import NewCinemaForm from './Forms/NewCinemaForm';
import EditCinemaForm from './Forms/EditCinemaForm';

class AdminCinemas extends Component {
  constructor(props){
    super(props);
    this.state = {
      cinemas: null
    };
  }

  createCinemasTable = () => {
    let getCinemasXHR = axios({
        url: 'http://127.0.0.1/onlinekino/backend/Cinemas/get_cinemas',
        method: 'GET'
      });
    getCinemasXHR.then((data) => {
      if(!data.data.length){
        this.setState({
          movies: <tr><td>Ziadne kina!</td><td></td><td></td><td></td><td></td></tr>
        });
      }
      else{
        const cinemaList = data.data.map((cinema) => (
          <tr key={cinema.id}>
            <td>{cinema.name}</td>
            <td>{cinema.number_of_seats}</td>
            <td>{cinema.address}</td>
            <td>{cinema.description}</td>
            <td><EditCinemaForm updateTable={this.createCinemasTable} cinema={cinema}/><button onClick={() => {this.deleteCinema(cinema.id) }}>Zmazať</button></td>
          </tr>));
        this.setState({
          cinemas: cinemaList
        });
      }
    });
  }

  deleteCinema = (id) => {
    let getMoviesXHR = axios({
        url: 'http://127.0.0.1/onlinekino/backend/Cinemas/remove_cinema',
        method: 'POST',
        data: {
          id: id
        }
      });
    getMoviesXHR.then((data) => {
      this.createCinemasTable();
    });
  }

  componentDidMount() {
    this.createCinemasTable();
  }

  render() {
    return (
      <div>
        <button type="button" className="btn btn-success" data-toggle="modal" data-target="#addNewCinema">
          Pridať kino
        </button>
        <div>
          <h3>Kina:</h3>
          <div>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Názov</th>
                  <th>Počet sedadiel</th>
                  <th>Adresa</th>
                  <th>Popis</th>
                  <th>Akcie</th>
                </tr>
              </thead>
              <tbody>
                {this.state.cinemas}
              </tbody>
            </table>
          </div>
        </div>
        <NewCinemaForm updateTable={this.createCinemasTable}/>
      </div>
    );
  }
}

export default AdminCinemas;
