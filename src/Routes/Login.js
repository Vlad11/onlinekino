import React, { Component } from 'react';
import AuthForm from '../Forms/AuthForm.js';

class Login extends Component {
  render() {
  	const url = 'http://127.0.0.1/onlinekino/backend/Auth/login';

    return (
      <div>
      	<AuthForm submitButton={'Prihlásiť'} header={'Prihlásenie'} axios={{url:url}} />
      </div>
    );
  }
}

export default Login;
