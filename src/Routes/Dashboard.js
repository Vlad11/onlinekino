import React, { Component } from 'react';
import { Redirect } from "react-router-dom";

class Dashboard extends Component {
  render() {
  	let content = sessionStorage.getItem('name') !== 'admin' ? (<Redirect to={'/user/reservations'} />) : (<Redirect to={'/admin/movies'} />);
    return (
      <div>{content}</div>
    );
  }
}

export default Dashboard;
