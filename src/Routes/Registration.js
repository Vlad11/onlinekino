import React, { Component } from 'react';
import AuthForm from '../Forms/AuthForm.js';

class Registration extends Component {
  render() {
  	const url = 'http://127.0.0.1/onlinekino/backend/Auth/registration';

    return (
      <div>
      	<AuthForm submitButton={'Registrácia'} header={'Registrácia'} axios={{url:url}} />
      </div>
    );
  }
}

export default Registration;
