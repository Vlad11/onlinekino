import React, { Component } from 'react';
import axios from 'axios';
import {
  Link,
  Redirect
} from 'react-router-dom';

class ReservationInputContent extends Component {
	constructor(props){
    super(props);
	    this.state = {
	      warningMsg: '',
	      name: '',
	      reservation: null,
	      requestCancel: false,
	      notice: 'Poslana ziadost o zrusenie'
	    }
  	}

  	sendRequest = (id) =>{
  		let requestCancelReservationXHR = axios({
			url: 'http://127.0.0.1/onlinekino/backend/Reservations/request_cancel_reservation',
			method: 'POST',
			data: {
				id: id
			}
		});
		requestCancelReservationXHR.then((data) => {
			this.checkReservation();
			this.setState({
				requestCancel: true
			});
		});
  	}

  	displayReservation = (data) => {
      let button = null;
      let resList = data.map((d)=>{
        button = d.canceled === '2' ? (this.state.notice) : (<button onClick={() => {this.sendRequest(d.id)}}>Poslať žiadosť a zrušenie</button>);
        return (
          <tr key={d.id}><td>{d.movie}</td><td>{d.cinema}</td><td>{d.time}</td><td>{d.seat}</td><td>{button}</td>
          </tr>
        );
      });

  		return (<div>
  			<h4>Rezervacia na meno {this.state.name}</h4>
  			<table className="table">
  				<thead>
  					<tr>
  						<th>Film</th>
  						<th>Kino</th>
  						<th>Čas</th>
  						<th>Sedadlo</th>
  						<th>Akcie</th>
  					</tr>
  				</thead>
  				<tbody>
            {resList}
  				</tbody>
  			</table>

  			</div>);
  	}

  	checkReservation = () => {
  		let isInputEmpty = this.state.name.length === 0;
  		if(isInputEmpty){
  			this.setState({
  				warningMsg: 'Musíte zadať meno pre skontrolovanie rezervácie!'
  			});
  		}
  		else{
  			let checkReservationXHR = axios({
	  			url: 'http://127.0.0.1/onlinekino/backend/Reservations/check_by_name',
	  			method: 'POST',
	  			data: {
	  				name: this.state.name
	  			}
	  		});
	  		checkReservationXHR.then((data) => {
	  			if(data.data.length === 0){
	  				this.setState({
		  				warningMsg: `Na meno ${this.state.name} nie je žiadna rezervácia.`
		  			});
	  			}
	  			else{
	  				this.setState({
	  					reservation: this.displayReservation(data.data)
	  				});
	  			}
	  		});
  			this.setState({
  				warningMsg: ''
  			});
  		}
  	}

  	onChange = (e) => {
  		let value = e.target.value;
  		this.state.name = value;
  	}

	render() {
		return(
			<div>
				<div>
					<p>Do políčka zadajte vaše meno, na ktoré bola rezervácia uskutočnená.</p>
					<input type="text" onChange={this.onChange} />
					<button onClick={this.checkReservation}>Skontrolovať</button>
				</div>
				<div>
					<div>{this.state.reservation}</div>
					<div className="warning-msg">
						<p>{this.state.warningMsg}</p>
					</div>
				</div>
			</div>
		)
	}
}



class Reservations extends Component {
  render() {
  	const isLoggedIn = sessionStorage.getItem('logged');
  	let content = <Redirect to={'/user/reservations'} />;
  	if(!isLoggedIn){
  		content = <ReservationInputContent />
  	}
  	
    return (
      <div>
      	<h2>Rezervacie</h2>
      	{content}
      </div>
    );
  }
}

export default Reservations;
