import React, { Component } from 'react';
import {
  Link,
  Redirect
} from 'react-router-dom';

class UserLayout extends Component {
  render() {
    return (
      <div>
        <ul className="nav" style={{borderBottom: 'solid thin rgb(200,200,200)'}}>
          <li>
            <Link to="/user/reservations" className="nav-link">Moje rezervácie</Link>
          </li>
          <li className="nav-item">
            <Link to="/user/schedules" className="nav-link">Rozpisy</Link>
          </li>
        </ul>
        {this.props.children}
      </div>
    );
  }
}

export default UserLayout;
