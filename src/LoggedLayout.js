import React, { Component } from 'react';
import {
  Redirect
} from 'react-router-dom';
import axios from 'axios';
import AdminLayout from './AdminLayout';
import UserLayout from './UserLayout';

class LoggedLayout extends Component {
  constructor(props){
    super(...props);
    this.state = {
      redirect: false,
      isAdmin: false,
      adminMsg: ''
    };
  }

  logout = () => {
    sessionStorage.removeItem('logged');
    sessionStorage.removeItem('name');
    this.setState({
      redirect:true
    });
  }

  componentDidMount() {
    let userName = sessionStorage.getItem('name');
    let isAdminXHR = axios({
        url: 'http://127.0.0.1/onlinekino/backend/Auth/is_admin',
        method: 'POST',
        data: {
          name: userName
        }
      });
    isAdminXHR.then((data) => {
        let code = data.data;
        if(code){
          this.setState({isAdmin:code});
          this.setState({adminMsg: `(si ADMIN ;))`});
        }
      });
  }

  render() {
    if(this.state.redirect){
      this.state.redirect = false;
      return (<Redirect to={'/login'} />);
    }

    let layout = null;

    if(this.state.isAdmin){
      layout = (
        <AdminLayout>
          {this.props.children}
        </AdminLayout>);
    }
    else{
      layout = (
        <UserLayout>
          {this.props.children}
        </UserLayout>);
    }
    let userName = sessionStorage.getItem('name');
    return (
      <div>
        <div>
          <h1>Kinex</h1>
          <p>Vitaj {userName} {this.state.adminMsg}.</p>
          <button onClick={this.logout} style={{position:'absolute', right: '2em', top: '1em'}}>Odhlásiť</button>
        </div>
        {layout}
      </div>
    );
  }
}

export default LoggedLayout;
