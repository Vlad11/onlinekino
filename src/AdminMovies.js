import React, { Component } from 'react';
import {
  Link,
  Redirect
} from 'react-router-dom';
import axios from 'axios';
import NewMovieForm from './Forms/NewMovieForm';
import EditMovieForm from './Forms/EditMovieForm';

class AdminMovies extends Component {
  constructor(props){
    super(props);
    this.state = {
      movies: null
    };
  }

  createMoviesTable = () => {
    let getMoviesXHR = axios({
        url: 'http://127.0.0.1/onlinekino/backend/Movies/get_movies',
        method: 'GET'
      });
    getMoviesXHR.then((data) => {
      if(!data.data.length){
        this.setState({
          movies: <tr><td>Ziadne filmy!</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        });
      }
      else{
        const movieList = data.data.map((movie) => (
          <tr key={movie.id}>
            <td>{movie.name}</td>
            <td>{movie.year}</td>
            <td>{movie.country}</td>
            <td>{movie.actors}</td>
            <td>{movie.description}</td>
            <td>{movie.length}</td>
            <td>{movie.genre}</td>
            <td>{movie.director}</td>
            <td><EditMovieForm updateTable={this.createMoviesTable} movie={movie}/><button onClick={() => {this.deleteMovie(movie.id) }}>Zmazať</button></td>
          </tr>));
        this.setState({
          movies: movieList
        });
      }
    });
  }

  deleteMovie = (id) => {
    let getMoviesXHR = axios({
        url: 'http://127.0.0.1/onlinekino/backend/Movies/remove_movie',
        method: 'POST',
        data: {
          id: id
        }
      });
    getMoviesXHR.then((data) => {
      this.createMoviesTable();
    });
  }

  componentDidMount() {
    this.createMoviesTable();
  }

  render() {
    return (
      <div>
        <button type="button" className="btn btn-success" data-toggle="modal" data-target="#addNewMovie">
          Pridať film
        </button>
        <div>
          <h3>Filmy:</h3>
          <div>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>Názov</th>
                  <th>Rok</th>
                  <th>Krajina</th>
                  <th>Herci</th>
                  <th>Popis</th>
                  <th>Dĺžka (min)</th>
                  <th>Žáner</th>
                  <th>Režisér</th>
                  <th>Akcie</th>
                </tr>
              </thead>
              <tbody>
                {this.state.movies}
              </tbody>
            </table>
          </div>
        </div>
        <NewMovieForm updateTable={this.createMoviesTable}/>
      </div>
    );
  }
}

export default AdminMovies;
