import React from 'react';
import { Redirect } from "react-router-dom";

class LoggedInContainer extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
      isLoggedIn: false,
    }
  } 

  componentDidMount() {
    let isLogged = sessionStorage.getItem('logged');
    this.setState({isLoggedIn:isLogged});
  }

  render() {
    console.log(this.state.isLoggedIn);
    console.log(this.props.children);
    if(!this.state.isLoggedIn){
      return <Redirect to="/login" />;
    }
    return this.props.children;
  }
}

export default LoggedInContainer;