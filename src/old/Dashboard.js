import React from 'react';
import axios from 'axios';
import AdminLayout from './AdminLayout.js';
import AuthContent from './AuthContent.js';

const EditMovies = () => {
  return (
    <div>
      <h3>Editovanie filmov</h3>
      <button className="btn-default">Novy film</button>
    </div>
  );
}

const EditCinemas = () => {
  return (
    <div>
      <h3>Editovanie kin</h3>
      <button className="btn-default">Nove kino</button>
    </div>
  );
}

const EditSchedules = () => {
  return (
    <div>
      <h3>Editovanie rozpisov</h3>
      <button className="btn-default">Novy rozpis</button>
    </div>
  );
}

class DashboardContainer extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isAdmin: false,
      adminMsg: '',
      redirect: false,
      content: '',
    }
  } 

  componentDidMount() {
   /* let content = this.props.location.pathname.split('/')[2];
    this.setContent(content);*/

    let user = sessionStorage.getItem('name');

    //this.redirection();
    
    axios({
        url: 'http://127.0.0.1/onlinekino/backend/Auth/is_admin',
        method: 'POST',
        data: {
          name: user
        }
      }).then((data) => {
        let code = data.data;
        if(code){
          this.setState({isAdmin:code});
          this.setState({adminMsg: `${user} si prihlaseny ako admin.`});
        }
      });
  }

  setContent = (cont) => {
    console.log(cont);
    this.setState({content:cont});
  }

  displayContent = () => {
    console.log(this.state.content);
    switch(this.state.content){
      case 'edit-cinemas':
        return <EditCinemas />;
      case 'edit-movies':
        return <EditMovies />;
      case 'edit-schedules':
        return <EditSchedules />;
      default:
        return null; 
    }
  }

  /*{(() => {
            if(this.state.isAdmin){
              return (
              <div>admin</div>
            )
            }
          })()}*/

  render() {   
    return (
      <AuthContent logout={this.props.logout}>
        <AdminLayout userName={sessionStorage.getItem('name')} contentName={'Dashboard'}>
          {this.displayContent()}
        </AdminLayout>
      </AuthContent>
      );
  }
}

export default DashboardContainer;