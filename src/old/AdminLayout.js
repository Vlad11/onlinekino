import React from 'react';
import { BrowserRouter as Router, Link} from "react-router-dom";

class AdminLayout extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isAdmin: false,
      adminMsg: ''
    }
  }

  componentDidMount() {
  }

  render() {
  	return (
  		<div>
        <p>Ahoj {this.props.userName}, si prihlaseny ako admin!</p>
        <h3>{this.props.contentName}</h3>
        <Router>
          <ul className="nav">
            <li className="nav-item">
              <Link to="/dashboard/edit-cinemas" onClick={() => this.setContent('edit-cinemas')}>Editovanie Kin</Link>
            </li>
            <li>
              <Link to="/dashboard/edit-movies" onClick={() => this.setContent('edit-movies')}>Editovanie Filmov</Link>
            </li>
            <li>
              <Link to="/dashboard/edit-schedules" onClick={() => this.setContent('edit-schedules')}>Editovanie Rozpisov</Link>
            </li>
          </ul>
        </Router>
        {this.props.children}
    	</div>
  	);
  }
} 

export default AdminLayout;