import React, { Component } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Link} from "react-router-dom";
import AuthForm from './AuthForm.js';
import DashboardContainer from './Dashboard.js';



const Home = () => (
  <div>
    <h2>Home</h2>
    Momentalne sa tu nenachadza ziadny rozpis
  </div>
);

const axiosProp = {
  url: {
    registration: 'http://127.0.0.1/onlinekino/backend/Auth/registration',
    login: 'http://127.0.0.1/onlinekino/backend/Auth/login'
  }
}

const registrationContent = (links) => {
  return (<div>{links()}<AuthForm submitButton={'Registrovat'} header={'Registracia'} axios={{url:axiosProp.url.registration}}/></div>);
};

const loginContent = (login,links) => {
  return (<div>{links()}<AuthForm submitButton={'Prihlasit'} header={'Prihlasenie'} axios={{url:axiosProp.url.login}} login={login}/></div>);
};

class AppLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoggedIn:false,
      inApp: false
    }
  }

  isLoggedIn = () => {
    if(!sessionStorage.getItem('logged')){
      this.setState({isLoggedIn:false});
    }
    else{
      this.setState({isLoggedIn:true});
    }
  }

  setLogOut = () => {
    this.setState({isLoggedIn:false});
  }

  setLogIn = () => {
    this.setState({isLoggedIn:true});
  }

  links = () => {
    return (
      <ul className="nav">
        <li className="nav-item">
          <Link to="/">Domov</Link>
        </li>
        <li>
          <Link to="/login">Prihlasenie</Link>
        </li>
        <li>
          <Link to="/registration">Registracia</Link>
        </li>
      </ul>
    );
  }


  displayLinks = () => {
    if(!this.state.isLoggedIn){
      this.links();
    }
    return (null);
  }

  componentDidMount() {
    this.isLoggedIn();
  }

  render() {
    return (
      <Router>
        <div>
          {this.displayLinks()}

          <Route exact path="/" component={Home} />
          <Route path="/login" render={() => (loginContent(this.setLogIn,this.links))} />
          <Route path="/registration" render={() => (registrationContent(this.links))} />
          <Route path="/dashboard" render={() => (<DashboardContainer login={this.setLogIn} logout={this.setLogOut}/>)} />
        </div>
      </Router>
    );
  }
}

export default AppLayout;
