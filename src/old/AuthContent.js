import React from 'react';
import { Redirect} from "react-router-dom";

class AuthContent extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: false
    }
  }

  redirection = ()  => {
    if(!sessionStorage.getItem('logged')){
      this.setState({
        redirect:true
      });
    }
  }

  logout = () => {
    sessionStorage.removeItem('logged');
    sessionStorage.removeItem('name');
    this.setState({
      redirect:true
    });
    this.props.logout();
  }

  componentDidMount() {
    this.redirection();
  }

  render() {
    console.log(this.props);
    if (this.state.redirect) {
      return <Redirect to='/login' />;
    }
  	return (
  		<div>
        <button className="button" onClick={this.logout}>Odhlasit</button>
        {this.props.children}
    	</div>
  	);
  }
} 

export default AuthContent;