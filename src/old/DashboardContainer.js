import React from 'react';

class DashboardTemplate extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isAdmin: false,
      adminMsg: ''
    }
}

    render() {
    	return (
    		<div>
	    		<h2>Dashboard 2</h2>
	    		this.props.children;
	    	</div>
    	);
    }
} 

export default DashboardTemplate;