import React, { Component } from 'react';
class Password extends Component {
  constructor(props){
    super(props);
    this.state = {
      msg:'',
      value: '',
      isValid: false
    }
  }

  validate = (event) => {
    let msgs = {
      notLength: 'Heslo musi mat najmenej 5 znakov',
      ok: ''
    }
    let reg = /(\w|\W){5,30}$/;
    this.state.value = event.target.value;

    if(!reg.test(this.state.value)){
      this.state.isValid = false
      this.setState({
        msg:msgs.notLength
      });
    }
    else{
      this.state.isValid = true
      this.setState({
        msg:msgs.ok
      });
    }
  }

  onChange = (event) => {
    this.validate(event);
    this.props.onChange(this.props.name,this.state.value,this.state.isValid);
  }

  render(){
    return(
      <div>
        <label>
          * Heslo:
          <input type='password'  className="form-control" onChange={this.onChange} />
        </label>
        <div className="warning-msg">{this.state.msg}</div>
      </div>
    )
  }
}

export default Password;