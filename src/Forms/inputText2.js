import React, { Component } from 'react';
class TextInput2 extends Component {
  constructor(props){
    super(props);
    this.state = {
      msg:'',
      value: '',
      isValid: false
    }
  }

  validate = (event) => {
    let msgs = {
      notLength: 'Pole nesmie byt prazdne',
      ok: ''
    }
    let notLength = this.state.value.length === 0;


    if(notLength){
      this.state.isValid = false;
      this.setState({
        msg:msgs.notLength
      });
    }
    else{
      this.state.isValid = true;
      this.setState({
        msg:msgs.ok
      });
    }
  }

  onChange = (event) => {
    let valid = 'x';
    this.state.value = event.target.value;
    if(this.props.validation){
      this.validate(event);
      valid = this.state.isValid;
    }
    this.props.onChange(this.props.name,this.state.value,valid);
  }

  render(){
    let type = this.props.type || 'text';
    return(
      <div>
        <label>
          {this.props.label}:
          <input type={type} className="form-control" onChange={this.onChange} value={this.props.value} /> {this.props.labelPost}
        </label>
        <div className="warning-msg">{this.state.msg}</div>
      </div>
    )
  }
}

export default TextInput2;