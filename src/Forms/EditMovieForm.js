import React, { Component } from 'react';
import axios from 'axios';
import Password from './inputPassword';
import TextInput2 from './inputText2';
import YearInput from './yearInput';
import { Redirect,  } from "react-router-dom";

class EditMovieForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      msg:'',
      nameValue: props.movie.name,
      nameValid: true,
      yearValue: props.movie.year,
      yearValid: true,
      countryValue: props.movie.country,
      actorsValue: props.movie.actors,
      descriptionValue: props.movie.description,
      lengthValue: props.movie.length,
      genreValue: props.movie.genre,
      directorValue: props.movie.director,
      directorValid: true,
      warningMsg: ''
    }
  }

  onChange = (name,value,valid) => {
  	let nameInput = `${name}Value`;
    this.setState({
      [nameInput]: value
    });
    if(valid !== 'x'){
      let validInput = `${name}Valid`;
      this.state[validInput] = valid;
    }
  }

  validation = () =>{
  	return this.state.directorValid && this.state.nameValid && this.state.yearValid;
  }

  setToDefault = () => {
    this.setState({
      warningMsg: ''
    });  
  }

  submitForm = (e) => {
  	e.preventDefault();
    this.state.warningMsg = '';
  	if(this.validation()){

      let updateMovieXHR = axios({
        url: 'http://127.0.0.1/onlinekino/backend/Movies/update_movie',
        method: 'POST',
        data: {
          id: this.props.movie.id,
          name: this.state.nameValue,
          year: this.state.yearValue,
          country: this.state.countryValue,
          actors: this.state.actorsValue,
          description: this.state.descriptionValue,
          length: this.state.lengthValue,
          genre: this.state.genreValue,
          director: this.state.directorValue
        }
      });

      updateMovieXHR.then((data) => {
        let code = data.data;
        if(!code){
          this.setState({
            warningMsg: 'Problém s uložením do databázi!'
          });
        }
        else{
          this.setState({
            warningMsg: 'Film bol úspešne zmenený.'
          });
        }
        this.props.updateTable();
      });
  	}
  	else{
  		this.setState({
  			warningMsg: 'Niektoré políčka vo formulári nie sú správne vyplnené!'
  		});
  	}
  }

  render(){
    let idModal = `editMovie${this.props.movie.id}`;
    let idModal2 = '#'+idModal;
  	return(
      <div>
        <button data-toggle="modal" data-target={idModal2}>Upraviť</button>

    		<div className="modal fade" id={idModal} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <form onSubmit={this.submitForm}>
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLabel">Upravit film</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                  
                    <div className="form-group">
                    </div>
                    <div className="form-group">
                      <TextInput2 onChange={this.onChange} name={'name'} label={'* Nazov'} validation value={this.state.nameValue}/>
                      <YearInput onChange={this.onChange} name={'year'} label={'* Rok'} value={this.state.yearValue}/>
                      <TextInput2 onChange={this.onChange} name={'coutry'} label={'Krajina'} value={this.state.countryValue}/>
                      <TextInput2 onChange={this.onChange} name={'actors'} label={'Herci'} value={this.state.actorsValue}/>
                      <TextInput2 onChange={this.onChange} name={'description'} label={'Popis'} value={this.state.descriptionValue}/>
                      <TextInput2 onChange={this.onChange} name={'length'} label={'Dlzka'} type={'number'} labelPost={' min'} value={this.state.lengthValue} />
                      <TextInput2 onChange={this.onChange} name={'genre'} label={'Zaner'} value={this.state.genreValue}/>
                      <TextInput2 onChange={this.onChange} name={'director'} label={'* Reziser'} validation value={this.state.directorValue}/>
                    </div>
                     <p>{this.state.warningMsg}</p>
                </div>
                <div className="modal-footer">
                  <button type="submit" className="btn btn-primary" onClick={this.setToDefault}>Upravit film</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  	);
  }
}

export default EditMovieForm;





