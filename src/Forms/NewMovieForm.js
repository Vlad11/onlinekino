import React, { Component } from 'react';
import axios from 'axios';
import Password from './inputPassword';
import TextInput2 from './inputText2';
import YearInput from './yearInput';
import { Redirect,  } from "react-router-dom";

class NewMovieForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      msg:'',
      nameValue: '',
      nameValid: false,
      yearValue: '',
      yearValid: false,
      countryValue: '',
      actorsValue: '',
      descriptionValue: '',
      lengthValue: '',
      genreValue: '',
      directorValue: '',
      directorValid: false,
      warningMsg: '',
    }
  }

  onChange = (name,value,valid) => {
  	let nameInput = `${name}Value`;
    this.state[nameInput] = value;
    if(valid !== 'x'){
      let validInput = `${name}Valid`;
      this.state[validInput] = valid;
    }
  }

  validation = () =>{
  	return this.state.directorValid && this.state.nameValid && this.state.yearValid;
  }

  submitForm = (e) => {
  	e.preventDefault();
    this.state.warningMsg = '';
  	if(this.validation()){

      let insertMovieXHR = axios({
        url: 'http://127.0.0.1/onlinekino/backend/Movies/add_new_movie',
        method: 'POST',
        data: {
          name: this.state.nameValue,
          year: this.state.yearValue,
          country: this.state.countryValue,
          actors: this.state.actorsValue,
          description: this.state.descriptionValue,
          length: this.state.lengthValue,
          genre: this.state.genreValue,
          director: this.state.directorValue
        }
      });

      insertMovieXHR.then((data) => {
        let code = data.data;
        if(!code){
          this.setState({
            warningMsg: 'Problém s uložením do databázi!'
          });
        }
        else{
          this.setState({
            warningMsg: 'Film bol úspešne pridaný.'
          });
        }
        this.props.updateTable();
      });
  	}
  	else{
  		this.setState({
  			warningMsg: 'Niektoré políčka vo formulári nie sú správne vyplnené!'
  		});
  	}
  }

  render(){
  	return(
  		<div className="modal fade" id="addNewMovie" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <form onSubmit={this.submitForm}>
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">Nový film</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                
                  <div className="form-group">
                  </div>
                  <div className="form-group">
                    <TextInput2 onChange={this.onChange} name={'name'} label={'* Názov'} validation/>
                    <YearInput onChange={this.onChange} name={'year'} label={'* Rok'}/>
                    <TextInput2 onChange={this.onChange} name={'coutry'} label={'Krajina'}/>
                    <TextInput2 onChange={this.onChange} name={'actors'} label={'Herci'}/>
                    <TextInput2 onChange={this.onChange} name={'description'} label={'Popis'}/>
                    <TextInput2 onChange={this.onChange} name={'length'} label={'Dĺžka'} type={'number'} labelPost={' min'} />
                    <TextInput2 onChange={this.onChange} name={'genre'} label={'Žáner'}/>
                    <TextInput2 onChange={this.onChange} name={'director'} label={'* Režisér'} validation/>
                  </div>
                   <p>{this.state.warningMsg}</p>
              </div>
              <div className="modal-footer">
                <button type="submit" className="btn btn-primary">Pridať film</button>
              </div>
            </form>
          </div>
        </div>
      </div>
  	);
  }
}

export default NewMovieForm;





