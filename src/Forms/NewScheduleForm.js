import React, { Component } from 'react';
import axios from 'axios';
import { Redirect,  } from "react-router-dom";

class NewScheduleForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      msg:'',
      timeValue: '',
      timeValid: true,
      cinemaValue: '',
      movieValue: '',
      warningMsg: '',
      cinemaOptions: null,
      movieOptions: null
    }
  }

  onChange = (e) => {
  	let nameInput = `${e.target.name}Value`;
    this.state[nameInput] = e.target.value;
  }

  validation = () =>{
  	return this.state.timeValid && this.state.cinemaValue && this.state.movieValue;
  }

  createOption = (type) => {
    const partUrl = type === 'movie' ? 'Movies/get_movies' : 'Cinemas/get_cinemas';
    const label = type === 'movie' ? 'Film' : 'Kino';
    const name = type === 'movie' ? 'movie' : 'cinema';
    let option;
    let getOptionsXHR = axios({
        //url: '../../backend/'+partUrl,
        url: 'http://127.0.0.1/onlinekino/backend/'+partUrl,
        method: 'GET'
      });

      getOptionsXHR.then((data) => {
        let optList = data.data.map((d) => (
          <option value={d.id} key={d.id}>{d.name}</option>
        ));

        option = (<label>{label}<select className="form-control" onChange={this.onChange} name={name}>
          <option selected disabled>Vyber {label}</option>
          {optList}
          </select></label>);
        
        if(type === 'movie'){
          this.setState({
            movieOptions : option
          });
        }
        else{
          this.setState({
            cinemaOptions : option
          });
        }
      });
  }

  componentDidMount() {
    this.createOption('movie');
    this.createOption('cinema');
  }

  displayOption = () => {
    if(!this.state.cinemaOptions || !this.state.movieOptions){
      return (<div>Niektoré časti formularu sa načítavajú!</div>);
    }
    return (
      <div>
        <div>
          {this.state.cinemaOptions}
        </div>
        <div>
          {this.state.movieOptions}
        </div>
      </div>
    );
  }

  submitForm = (e) => {
  	e.preventDefault();
    this.state.warningMsg = '';
  	if(this.validation()){

      let insertScheduleXHR = axios({
        //url: '../../backend/Schedule/add_schedule';
        url: 'http://127.0.0.1/onlinekino/backend/Schedule/add_schedule',
        method: 'POST',
        data: {
          cinema: this.state.cinemaValue,
          movie: this.state.movieValue,
          time: '5.5.2012'
        }
      });

      insertScheduleXHR.then((data) => {
        let code = data.data;
        if(!code){
          this.setState({
            warningMsg: 'Problém s uložením do databázi!'
          });
        }
        else{
          this.setState({
            warningMsg: 'Rozpis bol úspešne pridaný.'
          });
        }
        this.props.updateTable();
      });
  	}
  	else{
  		this.setState({
  			warningMsg: 'Niektoré políčka vo formulári nie sú správne vyplnené!'
  		});
  	}
  }

  render(){
  	return(
  		<div className="modal fade" id="addNewSchedule" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <form onSubmit={this.submitForm}>
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">Nový rozpis</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                
                  <div className="form-group">
                  </div>
                  <div className="form-group">
                    {this.displayOption()}
                  </div>
                   <p>{this.state.warningMsg}</p>
              </div>
              <div className="modal-footer">
                <button type="submit" className="btn btn-primary">Pridať rozpis</button>
              </div>
            </form>
          </div>
        </div>
      </div>
  	);
  }
}

export default NewScheduleForm;





