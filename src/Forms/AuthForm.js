import React, { Component } from 'react';
import axios from 'axios';
import Password from './inputPassword';
import TextInput from './inputText';
import { Redirect } from "react-router-dom";

class AuthFrom extends Component {
  constructor(props){
    super(props);
    this.state = {
      msg:'',
      nameValue: '',
      nameValid: false,
      passwordValue: '',
      passwordValid: false,
      warningMsg: '',
      nextDestination: '/',
      redirect: false
    }
  }

  onChange = (name,value,valid) => {
  	let nameInput = `${name}Value`;
  	let validInput = `${name}Valid`;
    this.state[nameInput] = value;
    this.state[validInput] = valid;
  }

  validation = () =>{
  	return this.state.passwordValid && this.state.nameValid;
  }

  submitForm = (e) => {
  	e.preventDefault();
    this.state.warningMsg = '';
  	if(this.validation()){
  		axios({
  			url: this.props.axios.url,
  			method: 'POST',
  			data: {
  				name: this.state.nameValue,
  				password: this.state.passwordValue
  			}
  		}).then((data) => {
  			let code = data.data;
        this.state.redirect = false;
  			switch(code){
  				case 0:
            this.state.nextDestination = '/login';
  					this.setState({
  						redirect: true
  					});
  					break;
  				case 1:
			  		this.setState({
		  				warningMsg: 'Chyba v DB!'
		  			});
  					break;
  				case 2:
			  		this.setState({
		  				warningMsg: 'Uzivatel uz existuje!'
		  			});
  					break;
  				case 3: 
			  		this.setState({
		  				warningMsg: 'Nepodarilo sa vlozit noveho uzivatela!'
		  			});
  					break;
  				case 4: 
			  		this.setState({
		  				warningMsg: `Uzivatel s menom ${this.state.nameValue} neexistuje!`
		  			});
  					break;
  				case 5:
  					sessionStorage.setItem('logged', true);
  					sessionStorage.setItem('name', this.state.nameValue);
            this.state.nextDestination = '/dashboard';
  					this.setState({
  						redirect: true
  					});
  					break;
  				default:
  					break;
  			}
  		});
  	}
  	else{
  		this.setState({
  			warningMsg: 'Niektoré políčka vo formulári nie sú správne vyplnené!'
  		});
  	}
  }

  render(){
  	if (this.state.redirect) {
       return <Redirect to={this.state.nextDestination} />;
    }
  	return(
  		<div>
  			<h2>{this.props.header}</h2>
  			<div>
	          <form onSubmit={this.submitForm}>
	            <div className="form-group">
	            </div>
	            <div className="form-group">
	            	<TextInput onChange={this.onChange} name={'name'} label={'* Meno'}/>
	              	<Password onChange={this.onChange} name={'password'}/>
	            </div>
	             <p>{this.state.warningMsg}</p>
	            <button type="submit" className="btn btn-primary">{this.props.submitButton}</button>
	          </form>
	        </div>
	       
  		</div>
  	);
  }
}

export default AuthFrom;