import React, { Component } from 'react';
import axios from 'axios';
import Password from './inputPassword';
import TextInput2 from './inputText2';
import YearInput from './yearInput';
import { Redirect,  } from "react-router-dom";

class Seat extends Component {
  constructor(props){
    super(props);
    let reserved = this.props.reserved ? 1 : 0;
    this.state = {
      status:reserved,
      clicked:0
    };
  }

  onClick=()=>{
    if(this.state.status !== 1){
      this.state.clicked = !this.state.clicked;
      if(this.state.clicked){
        this.props.onClick(this.props.number,'add');
        this.setState({
          status:2,
        });
      }
      else{
        this.props.onClick(this.props.number,'remove');
        this.setState({
          status:0
        });
      }
      
    }
    
  }

  createColor = () => {
    let color = '';
    switch(this.state.status){
      case(1):
        color = 'rgb(255,255,0)';
        break;
      case(2):
        color = 'rgb(157,124,13)';
        break;
      case(0):
      default:
        color = 'rgb(0,255,0)';
        break;
    }
    return color;
  }

  render(){
    return(<div style={{
      width:'30px',
      height:'20px',
      display:'inline-block',
      background: this.createColor(),
      margin: '2px',
      textAlign:'center',
      borderRadius: '3px'
    }} onClick={this.onClick}>{this.props.number}</div>);
  }
}

class NewReservationForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      msg:'',
      name: '',
      seat:'',
      warningMsg: '',
      seats:null,
      displayButton: true
    }
  }

  onChange = (e) => {
  	let n = e.target.name;
    this.setState({
      [n]: e.target.value
    });
  }

  validation = () =>{
    if(sessionStorage.getItem('name')){
      return true;
    }
  	return (this.state.name.length > 0) && (this.state.seat.length > 0);
  }

  setToDefault = () => {
    this.state.seats = null;
    this.setState({
      warningMsg: ''
    });  
  }

  submitForm = (e) => {
  	e.preventDefault();
    this.state.warningMsg = '';
  	if(this.validation()){
      const name = sessionStorage.getItem('name') || this.state.name;
      const url = sessionStorage.getItem('name') ? 'create_reservation_by_name_logged' : 'create_reservation_by_name';

      let makeReservationXHR = axios({
        url: 'http://127.0.0.1/onlinekino/backend/Reservations/'+url,
        method: 'POST',
        data: {
          id: this.props.scheduleId,
          seat: this.state.seat,
          name: name,
        }
      });

      makeReservationXHR.then((data) => {
        let code = data.data;
        if(code === 0){
          this.setState({
            warningMsg: 'Problem s uložením do databázi!'
          });
        }
        else if(code === 2){
          this.setState({
            warningMsg: 'Toto meno je už použité!'
          });
        }
        else{
          this.setState({
            warningMsg: 'Rezervácia bola vytvorená.'
          });
        }
        this.props.updateTable();
        this.getReservedSeats();
      });
  	}
  	else{
  		this.setState({
  			warningMsg: 'Niektoré políčka vo formulári nie sú správne vyplnené!'
  		});
  	}
  }

  selectSeat=(number,action)=>{
    let value = this.state.seat;
    if(action==='add'){
      value += ' '+number;
    }
    else{
      value.replace(number,'');
    }
    this.setState({
      seat:value
    });
  }

   getReservedSeats=()=>{
    let reservedSeatsXHR = axios({
        url: 'http://127.0.0.1/onlinekino/backend/Reservations/get_reserved_seats',
        method: 'POST',
        data: {
          schedule: this.props.scheduleId
        }
      });

      reservedSeatsXHR.then((data) => {
        let d = data.data;
        console.log(d,'ahaa');
        let map = {};

        for(let i =0, length = d.length; i < length; i++){
          let k = d[i].seat.split(' ');
          let kk = k.filter(h => h.length > 0);
          for(let j = 0, len = kk.length; j < len; j++){
            if(kk[j]!=0){
              map[kk[j]] = null;
            }
          }
        }
        this.createSeats(map);
      });
  }

  createSeats=(reservedSeats)=>{
    console.log(reservedSeats);
    let number = parseInt(this.props.numberOfSeats);
    console.log(Object.keys(reservedSeats).length,number);
    if(!number || Object.keys(reservedSeats).length === number){
      this.state.displayButton = false;
      this.setState({
        seats: <p>Nie su žiadne voľné sedadlá</p>
      });
    }
    else{
      let a = Array.from(Array(number).keys());
      let seats = a.map((i)=>{
        if((i+1) in reservedSeats){
          return (<Seat number={i+1} key={i} onClick={this.selectSeat} reserved/>);
        }
        else{
          return (<Seat number={i+1} key={i} onClick={this.selectSeat}/>);
        }
      });

      let cinemaPlace = function(seats){
        return (<div style={{width:'300px',margin:'0 25%'}}><div>{seats}</div><div style={{
          width: '75%',
          height: '5px',
          display: 'block',
          margin: '0 auto',
          background: 'rgb(157,124,13)'
        }}></div></div>);
      };

      this.setState({
        seats:cinemaPlace(seats)
      });
    }

  };

  componentDidMount(){
    this.getReservedSeats();
  }

  render(){
    let idModal = `newReservation${this.props.scheduleId}`;
    let idModal2 = '#'+idModal;
    let isLoggedIn = sessionStorage.getItem('name') ? true : false;
    let displayName = isLoggedIn ? (<span>Si prihlaseny ako {sessionStorage.getItem('name')}. Pod tymto menom bude vykonana rezervacia.</span>) : (<label>Zadaj meno: <input type="text" name="name" onChange={this.onChange}/></label>);
  	return(
      <div>
        <button data-toggle="modal" data-target={idModal2}>Urobiť rezerváciu</button>

    		<div className="modal fade" id={idModal} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <form onSubmit={this.submitForm}>
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLabel">Urobit rezervaciu</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                    <div className="form-group">
                      {displayName}
                      <div>
                        {this.state.seats}
                      </div>
                    </div>
                     <p>{this.state.warningMsg}</p>
                </div>
                <div className="modal-footer">
                  {this.state.displayButton ? (<button type="submit" className="btn btn-primary" onClick={this.setToDefault}>Rezervuj</button>) : (<a className="close btn" data-dismiss="modal">Zavrieť</a>)}
                  
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  	);
  }
}

export default NewReservationForm;





