import React, { Component } from 'react';
class YearInput extends Component {
  constructor(props){
    super(props);
    this.state = {
      msg:'',
      value: '',
      isValid: false
    }
  }

  validate = (event) => {
    let msgs = {
      notLength: 'Pole musi mat dlzku 4',
      noDate: 'Hodnota nie je rok',
      ok: ''
    }
    this.state.value = event.target.value;
    let notLength = this.state.value.length !== 4;
    let reg = /^[0-9]{4,4}$/;  

    if(!reg.test(this.state.value) || notLength){
      this.state.isValid = false;
      if(notLength){
        this.setState({
          msg:msgs.notLength
        });
      }
      else{
        this.setState({
          msg:msgs.noDate
        });
      }
    }
    else{
      this.state.isValid = true;
      this.setState({
        msg:msgs.ok
      });
    }
  }

  onChange = (event) => {
    this.validate(event);
    this.props.onChange(this.props.name,this.state.value,this.state.isValid);
  }

  render(){
    return(
      <div>
        <label>
          {this.props.label}:
          <input type='text' className="form-control" onChange={this.onChange} value={this.props.value} />
        </label>
        <div className="warning-msg">{this.state.msg}</div>
      </div>
    )
  }
}

export default YearInput;