import React, { Component } from 'react';
import axios from 'axios';
import Password from './inputPassword';
import TextInput2 from './inputText2';

class NewCinemaForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      msg:'',
      nameValue: '',
      nameValid: false,
      numberOfSeatsValue: 0,
      descriptionValue: '',
      addressValue: '',
      addressValid: false,
      warningMsg: '',
    }
  }

  onChange = (name,value,valid) => {
  	let nameInput = `${name}Value`;
    this.state[nameInput] = value;
    if(valid !== 'x'){
      let validInput = `${name}Valid`;
      this.state[validInput] = valid;
    }
  }

  validation = () =>{
  	return this.state.addressValid && this.state.nameValid;
  }

  submitForm = (e) => {
  	e.preventDefault();
    this.state.warningMsg = '';
  	if(this.validation()){

      let insertCinemaXHR = axios({
        url: 'http://127.0.0.1/onlinekino/backend/Cinemas/add_new_cinema',
        method: 'POST',
        data: {
          name: this.state.nameValue,
          address: this.state.addressValue,
          numberOfSeats: this.state.numberOfSeatsValue,
          description: this.state.descriptionValue
        }
      });

      insertCinemaXHR.then((data) => {
        let code = data.data;
        if(!code){
          this.setState({
            warningMsg: 'Problem s uložením do databázi!'
          });
        }
        else{
          this.setState({
            warningMsg: 'Kino bolo úspešne pridané.'
          });
        }
        this.props.updateTable();
      });
  	}
  	else{
  		this.setState({
  			warningMsg: 'Niektoré políčka vo formulári nie sú správne vyplnené!'
  		});
  	}
  }

  render(){
  	return(
  		<div className="modal fade" id="addNewCinema" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <form onSubmit={this.submitForm}>
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">Novy film</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                
                  <div className="form-group">
                  </div>
                  <div className="form-group">
                    <TextInput2 onChange={this.onChange} name={'name'} label={'* Názov'} validation/>
                    <TextInput2 onChange={this.onChange} name={'numberOfSeats'} label={'Počet sedadiel'} type={'number'} />
                    <TextInput2 onChange={this.onChange} name={'address'} label={'* Adresa'} validation/>
                    <TextInput2 onChange={this.onChange} name={'description'} label={'Popis'}/>
                  </div>
                   <p>{this.state.warningMsg}</p>
              </div>
              <div className="modal-footer">
                <button type="submit" className="btn btn-primary">Pridať kino</button>
              </div>
            </form>
          </div>
        </div>
      </div>
  	);
  }
}

export default NewCinemaForm;





