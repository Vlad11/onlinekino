import React, { Component } from 'react';
class TextInput extends Component {
  constructor(props){
    super(props);
    this.state = {
      msg:'',
      value: '',
      isValid: false
    }
  }

  validate = (event) => {
    let msgs = {
      notLength: 'Heslo musi mat najmenej 2 znakov',
      alfaNum: 'Heslo musi obsahovat pismena alebo cisla',
      ok: ''
    }
    let reg = /^[0-9a-zA-Z\.]{3,30}$/;
    this.state.value = event.target.value;

    if(!reg.test(this.state.value)){
      this.state.isValid = false;
      if(this.state.value < 3){
        this.setState({
          msg:msgs.notLength
        });
      }
      else{
        this.setState({
          msg:msgs.alfaNum
        });
      }
    }
    else{
      this.state.isValid = true;
      this.setState({
        msg:msgs.ok
      });
    }
  }

  onChange = (event) => {
    this.validate(event);
    this.props.onChange(this.props.name,this.state.value,this.state.isValid);
  }

  render(){
    return(
      <div>
        <label>
          {this.props.label}:
          <input type='text' className="form-control" onChange={this.onChange} value={this.props.value} />
        </label>
        <div className="warning-msg">{this.state.msg}</div>
      </div>
    )
  }
}

export default TextInput;