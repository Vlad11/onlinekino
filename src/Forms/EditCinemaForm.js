import React, { Component } from 'react';
import axios from 'axios';
import Password from './inputPassword';
import TextInput2 from './inputText2';

class EditCinemaForm extends Component {
  constructor(props){
    super(props);
    this.state = {
      msg:'',
      nameValue: props.cinema.name,
      nameValid: true,
      numberOfSeatsValue: props.cinema.number_of_seats,
      addressValue: props.cinema.address,
      addressValid: true,
      descriptionValue: props.cinema.description,
      warningMsg: ''
    }
  }

  onChange = (name,value,valid) => {
  	let nameInput = `${name}Value`;
    this.setState({
      [nameInput]: value
    });
    if(valid !== 'x'){
      let validInput = `${name}Valid`;
      this.state[validInput] = valid;
    }
  }

  validation = () =>{
  	return this.state.addressValid && this.state.nameValid;
  }

  setToDefault = () => {
    this.setState({
      warningMsg: ''
    });  
  }

  submitForm = (e) => {
  	e.preventDefault();
    this.state.warningMsg = '';
  	if(this.validation()){

      let updateMovieXHR = axios({
        url: 'http://127.0.0.1/onlinekino/backend/Cinemas/update_cinema',
        method: 'POST',
        data: {
          id: this.props.cinema.id,
          name: this.state.nameValue,
          numberOfSeats: this.state.numberOfSeatsValue,
          address: this.state.addressValue,
          description: this.state.descriptionValue
        }
      });

      updateMovieXHR.then((data) => {
        let code = data.data;
        if(!code){
          this.setState({
            warningMsg: 'Problém s uložením do databazi!'
          });
        }
        else{
          this.setState({
            warningMsg: 'Film bol úspešne zmenený.'
          });
        }
        this.props.updateTable();
      });
  	}
  	else{
  		this.setState({
  			warningMsg: 'Niektoré políčka vo formulári nie sú správne vyplnené!'
  		});
  	}
  }

  render(){
    let idModal = `editCinema${this.props.cinema.id}`;
    let idModal2 = '#'+idModal;
  	return(
      <div>
        <button data-toggle="modal" data-target={idModal2}>Upraviť</button>

    		<div className="modal fade" id={idModal} tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <form onSubmit={this.submitForm}>
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLabel">Upravit kino</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="modal-body">
                  
                    <div className="form-group">
                    </div>
                    <div className="form-group">
                      <TextInput2 onChange={this.onChange} name={'name'} label={'* Nazov'} validation value={this.state.nameValue}/>
                      <TextInput2 onChange={this.onChange} name={'numberOfSeats'} label={'Pocet sedadiel'} type={'number'} value={this.state.numberOfSeatsValue} />
                      <TextInput2 onChange={this.onChange} name={'address'} label={'Adresa'} value={this.state.addressValue} validation/>
                      <TextInput2 onChange={this.onChange} name={'description'} label={'Popis'} value={this.state.descriptionValue}/>
                    </div>
                     <p>{this.state.warningMsg}</p>
                </div>
                <div className="modal-footer">
                  <button type="submit" className="btn btn-primary" onClick={this.setToDefault}>Upravit kino</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
  	);
  }
}

export default EditCinemaForm;





