import React, { Component } from 'react';
import {
  Link,
  Redirect
} from 'react-router-dom';
import axios from 'axios';
import NewScheduleForm from './Forms/NewScheduleForm';
import EditMovieForm from './Forms/EditMovieForm';

class AdminSchedules extends Component {
  constructor(props){
    super(props);
    this.state = {
      schedules:null,
      details:null
    };
  }

  closeDetails = () => {
    this.setState({
      details:null
    });
  }

  displayDetails = (id) => {
    let getSchedulesXHR = axios({
          url: 'http://127.0.0.1/onlinekino/backend/Schedule/get_schedule_detail',
          method: 'POST',
          data: {
            id:id
          }
        });
      getSchedulesXHR.then((data) => {
        let d = data.data[0];
        let detailContent = (
          <div className="panel panel-default" style={{borderTop:'solid thin rgb(200,200,200)'}}>
          <div className="panel-heading">Detail filmu {d.name} v kine {d.cinema_name}</div>
          <div className="panel-body" style={{width:'500px',position:'relative'}}>
            <span onClick={this.closeDetails} title="Zavrieť" style={{cursor:'pointer', fontSize:'20px',position:'absolute',right:'1em',top:'-1em'}}>&times;</span>
              <ul>
                <li><strong>Film</strong> - {d.name}</li>
                <li><strong>Popis</strong> - {d.description}</li>
                <li><strong>Režisér</strong> - {d.director}</li>
                <li><strong>Herci</strong> - {d.actors}</li>
                <li><strong>Žáner</strong> - {d.genre}</li>
                <li><strong>Krajina</strong> - {d.country}</li>
                <li><strong>Dĺžka</strong> - {d.length} min</li>
                <li><strong>Rok</strong> - {d.year}</li>
                <li><strong>Čas premietania</strong> - {d.time}</li>
                <li><strong>Kino</strong> - {d.cinema_name}</li>
                <li><strong>Popis</strong> - {d.cinema_description}</li>
                <li><strong>Počet sedadiel</strong> - {d.seats}</li>
                <li><strong>Adresa</strong> - {d.adress}</li>
              </ul>
          </div>
        </div>
        );

        this.setState({
          details:detailContent
        });
      });
  }


  deleteSchedule = (id) => {
    let getSchedulesXHR = axios({
        url: 'http://127.0.0.1/onlinekino/backend/Schedule/remove_schedule',
        method: 'POST',
        data: {
          id:id
        }
      });
    getSchedulesXHR.then((data) => {
      this.closeDetails();
      this.createSchedulesTable();
    });
  }

  createSchedulesTable = () => {
      let getSchedulesXHR = axios({
          url: 'http://127.0.0.1/onlinekino/backend/Schedule/get_schedules',
          method: 'GET'
        });
      getSchedulesXHR.then((data) => {
        if(!data.data.length){
          this.setState({
            schedules: <tr><td>Žiadne rezervácie!</td></tr>
          });
        }
        else{
          const schedList = data.data.map((sched) => (
            <tr key={sched.id}>
              <td onClick={() => {this.displayDetails(sched.id)}}>{sched.movie}</td>
              <td onClick={() => {this.displayDetails(sched.id)}}>{sched.cinema}</td>
              <td onClick={() => {this.displayDetails(sched.id)}}>{sched.time}</td>
              <td onClick={() => {this.displayDetails(sched.id)}}>{sched.length} min</td>
              <td onClick={() => {this.displayDetails(sched.id)}}>{sched.director}</td>
              <td><button onClick={() => {this.deleteSchedule(sched.id) }}>Vymazať</button></td>
            </tr>));
          this.setState({
            schedules: schedList
          });
        }
      });
    }

  componentDidMount(){
    this.createSchedulesTable();
  }

  render() {
    return (
        <div>
          <div>
            <button type="button" className="btn btn-success" data-toggle="modal" data-target="#addNewSchedule">
              Pridať rozpis
            </button>
          </div>
          <div>
            <h3>Rezervácie:</h3>
              <div>
                <div>
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th>Film</th>
                        <th>Kino</th>
                        <th>Čas</th>
                        <th>Dĺžka filmu</th>
                        <th>Režisér</th>
                        <th>Akcie</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.schedules}
                    </tbody>
                  </table>
                 </div>
                 <div>
                 {this.state.details}
                 </div>
              </div>
            </div>
            <NewScheduleForm updateTable={this.createSchedulesTable} />
        </div>
    );
  }
}

export default AdminSchedules;
