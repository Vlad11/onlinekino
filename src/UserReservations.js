import React, { Component } from 'react';
import {
  Link,
  Redirect
} from 'react-router-dom';
import axios from 'axios';
import NewMovieForm from './Forms/NewMovieForm';
import EditMovieForm from './Forms/EditMovieForm';

class UserReservations extends Component {
  constructor(props){
    super(props);
    this.state = {
      reservations: null,
      status : {
        0: 'Aktivna',
        1: 'Zrusena',
        2: 'Cakajuca na zrusenie'
      }
    };
  }

  createReservationsTable = () => {
    let getMoviesXHR = axios({
        url: 'http://127.0.0.1/onlinekino/backend/Reservations/get_user_reservations',
        method: 'POST',
        data: {
          name:sessionStorage.getItem('name')
        }
      });
    getMoviesXHR.then((data) => {
      if(!data.data.length){
        this.setState({
          reservations: <tr><td>Ziadne rezervacie!</td><td></td><td></td><td></td><td></td><td></td></tr>
        });
      }
      else{
        const resList = data.data.map((res) => {
          return (
          <tr res={res.id}>
            <td>{res.seat}</td>
            <td>{res.time}</td>
            <td>{res.movie}</td>
            <td>{res.cinema}</td>
            <td>{this.state.status[res.canceled]}</td>
            <td>{res.canceled === '0' ? (<button onClick={() => {this.sendRequest(res.id) }}>Zrusit rezervaciu</button>) : ''}</td>
          </tr>)});
        this.setState({
          reservations: resList
        });
      }
    });
  }

  sendRequest = (id) =>{
      let requestCancelReservationXHR = axios({
      url: 'http://127.0.0.1/onlinekino/backend/Reservations/request_cancel_reservation',
      method: 'POST',
      data: {
        id: id
      }
    });
    requestCancelReservationXHR.then((data) => {
      this.createReservationsTable();
    });
  }

  componentDidMount() {
    this.createReservationsTable();
  }

  render() {
    return (
      <div>
        <h3>Rezervacie:</h3>
        <div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Sedadlo</th>
                <th>Cas</th>
                <th>Film</th>
                <th>Kino</th>
                <th>Stav</th>
                <th>Akcie</th>
              </tr>
            </thead>
            <tbody>
              {this.state.reservations}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default UserReservations;
