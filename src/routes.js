import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
  Link
} from 'react-router-dom';
import React, { Component}  from 'react';
import Login from './Routes/Login';
import Registration from './Routes/Registration';
import Schedules from './Routes/Schedules';
import Reservations from './Routes/Reservations';
import Dashboard from './Routes/Dashboard';
import AppLayout from './AppLayout';
import LoggedLayout from './LoggedLayout';
import AdminMovies from './AdminMovies';
import AdminCinemas from './AdminCinemas';
import AdminReservations from './AdminReservations';
import AdminSchedules from './AdminSchedule';
import UserReservations from './UserReservations';

const Root = (<div>Vitajte na stranke Kinex. Tato stranka poskytuje rychle a jednoduche zarezervovanie listkov do kina.</div>);

const AppLayoutComponent = (InnerComponent) => {
	return (

	<AppLayout>
		{InnerComponent.component}
	</AppLayout>
)};

const PrivateRoute = ({ component: Component, ...rest }) => {
  const loggedIn = sessionStorage.getItem('logged');
  return(
  <Route
    {...rest}
    render={props =>
      loggedIn ? (
        <LoggedLayout>
          <Component {...props} />
        </LoggedLayout>
      ) : (
        <Redirect
          to={{
            pathname: "/login",
            state: { from: props.location }
          }}
        />
      )
    }
  />
)};

const NoMatch = ({ location }) => {
  let locale = location.pathname;
  let nextSteps = '';

  switch(locale){
    case '/admin':
      nextSteps = <div>Mozno ste mali na mysli: <Link to={'/admin/movies'}>Administracia kin</Link><Link to={'/admin/schedules'}>Administracia rozpisov kin</Link><Link to={'/admin/cinemas'}>Administracia filmov</Link></div>;
  }
  return (
  <div>
    <h3>404 - No match for <code>{locale}</code></h3>
    {nextSteps}
  </div>
)}

export default (
	<Router>
		<Switch>
      <Route exact path='/' render={()=>(<AppLayoutComponent component={Root} />)} />
			<Route path='/login' render={() => (<AppLayoutComponent component={<Login/>} />)} />
			<Route path='/registration' render={() => (<AppLayoutComponent component={<Registration/>} /> )} />
			<Route path='/schedules' render={() => (<AppLayoutComponent component={<Schedules/>} /> )} />
			<Route exact path='/reservations' render={() => (<AppLayoutComponent component={<Reservations/>} /> )} />
			<PrivateRoute path="/dashboard" component={Dashboard} />
      <PrivateRoute path="/admin/movies" component={AdminMovies} />
      <PrivateRoute path="/admin/cinemas" component={AdminCinemas} />
      <PrivateRoute path="/admin/reservations" component={AdminReservations} />
      <PrivateRoute path="/admin/schedules" component={AdminSchedules} />
      <PrivateRoute path="/user/reservations" component={UserReservations} />
      <PrivateRoute path="/user/schedules" component={Schedules} />
      <Route component={NoMatch} />
		</Switch>
	</Router>
); 