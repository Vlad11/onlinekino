import React, { Component } from 'react';
import {
  Link,
  Redirect
} from 'react-router-dom';
import axios from 'axios';
import NewMovieForm from './Forms/NewMovieForm';
import EditMovieForm from './Forms/EditMovieForm';

class AdminReservations extends Component {
  constructor(props){
    super(props);
    this.state = {
      reservations: null
    };
  }

  createReservationsTable = () => {
    let getMoviesXHR = axios({
        url: 'http://127.0.0.1/onlinekino/backend/Reservations/get_active_reservations',
        method: 'GET'
      });
    getMoviesXHR.then((data) => {
      if(!data.data.length){
        this.setState({
          reservations: <tr><td>Ziadne rezervacie!</td><td></td><td></td><td></td><td></td></tr>
        });
      }
      else{
        const resList = data.data.map((res) => (
          <tr res={res.id}>
            <td>{res.user_name}</td>
            <td>{res.seat}</td>
            <td>{res.time}</td>
            <td>{res.movie}</td>
            <td>{res.cinema}</td>
            <td><button onClick={() => {this.deleteReservation(res.id) }}>Zrušiť rezerváciu</button>{ res.canceled === '2' ? (<span>Žiadosť o zrušenie</span>) : ''}</td>
          </tr>));
        this.setState({
          reservations: resList
        });
      }
    });
  }

  deleteReservation = (id) => {
    let cancelReservationXHR = axios({
        url: 'http://127.0.0.1/onlinekino/backend/Reservations/cancel_reservation',
        method: 'POST',
        data: {
          id: id
        }
      });
    cancelReservationXHR.then((data) => {
      this.createReservationsTable();
    });
  }

  componentDidMount() {
    this.createReservationsTable();
  }

  render() {
    return (
      <div>
        <h3>Rezervácie:</h3>
        <div>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Meno</th>
                <th>Sedadlo</th>
                <th>Čas</th>
                <th>Film</th>
                <th>Kino</th>
                <th>Akcie</th>
              </tr>
            </thead>
            <tbody>
              {this.state.reservations}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default AdminReservations;
