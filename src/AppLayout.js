import React, { Component } from 'react';
import {
  Link,
  Redirect
} from 'react-router-dom';

class AppLayout extends Component {
  render() {
    return (
      <div>
        <h1>Kinex</h1>
        <ul className="nav" style={{borderBottom: 'solid thin rgb(200,200,200)'}}>
          <li>
            <Link to="/registration" className="nav-link">Registrácia</Link>
          </li>
          <li>
            <Link to="/login" className="nav-link">Prihlásenie</Link>
          </li>
          <li>
            <Link to="/schedules" className="nav-link">Rozpis kín</Link>
          </li>
          <li>
            <Link to="/reservations" className="nav-link">Pozrieť rezerváciu</Link>
          </li>
        </ul>
        {this.props.children}
      </div>
    );
  }
}

export default AppLayout;
