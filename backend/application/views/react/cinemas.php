<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
	<?php
		$include_file = "assets/js/react/components/welcome.js";
	?>

	System.import('<?php echo $include_file; ?>').then(function(module) {
		var props = {};
		ReactDOM.render( React.createElement(module.default, props), document.getElementById('content') )
	})

</script>
