<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth_model extends CI_Model {

	public function is_unique($name){
		return !count($this->db->get_where('users', array('name' => $name))->result_array());
	}

	public function insert_new_user($name,$password){
		$data = array('name'=>$name,'password'=>$password);
		return $this->db->insert('users',$data);
	}

	public function get_id($name){
		$this->db->select('id');
		return $this->db->get('users',array('name'=>$name))->result_array()[0]['id'];
	}


	public function is_admin($name){
		return $this->db->get_where('users',array('name'=>$name,'type'=>1))->num_rows() == 1;
	}


}