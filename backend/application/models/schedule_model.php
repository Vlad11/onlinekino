<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Schedule_model extends CI_Model {

	public function get_schedules(){
		$this->db->select('s.id as id,s.time as time,m.name as movie,c.name as cinema,m.director as director,c.number_of_seats,c.id as cinema_id, m.length as length');
		$this->db->from('schedule as s');
		$this->db->join('cinema as c', 's.cinema_id = c.id');
		$this->db->join('movie as m', 's.movie_id = m.id');
		return $this->db->get()->result_array();
	}

	public function get_schedule_detail($id){
		$this->db->select('s.id,s.time,c.name as cinema_name,c.address,c.number_of_seats as seats,c.description as cinema_description,m.name,m.year,m.country,m.actors,m.description,m.length,m.genre,m.director');
		$this->db->from('schedule as s');
		$this->db->join('cinema as c', 's.cinema_id = c.id');
		$this->db->join('movie as m', 's.movie_id = m.id');
		$this->db->where('s.id',$id);
		return $this->db->get()->result_array();
	}

	public function remove_schedule($id){
		$this->db->where('id',$id);
		return $this->db->delete('schedule');
	}

	public function add_schedule($cinema,$movie,$time){
		$data = array('cinema_id'=>$cinema,'movie_id'=>$movie,'time'=>$time);
		return $this->db->insert('schedule',$data);
	}
}