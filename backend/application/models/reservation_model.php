<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reservation_model extends CI_Model {

	public function is_name_unique($name){
		return !count($this->db->get_where('reservations',array('name'=>$name))->result_array());
	}

	public function valid_name($name){
		return !count($this->db->get_where('users',array('name'=>$name))->result_array());
	}
	
	public function getByName($name){
		$this->db->select('r.id as id,r.seat as seat,s.time as time,m.name as movie,c.name as cinema,r.canceled');
		$this->db->from('reservations as r');
		$this->db->join('schedule as s', 'r.schedule_id = s.id');
		$this->db->join('cinema as c', 's.cinema_id = c.id');
		$this->db->join('movie as m', 's.movie_id = m.id');
		$this->db->where('r.name', $name);
		$this->db->where('r.canceled!=', 1);
		return $this->db->get()->result_array();
	}

	public function get_active_reservations(){
		$this->db->select('r.id as id,r.seat as seat,s.time as time,m.name as movie,c.name as cinema, r.name as user_name,r.canceled');
		$this->db->from('reservations as r');
		$this->db->join('schedule as s', 'r.schedule_id = s.id');
		$this->db->join('cinema as c', 's.cinema_id = c.id');
		$this->db->join('movie as m', 's.movie_id = m.id');
		$this->db->where('canceled!=', 1);
		return $this->db->get()->result_array();
	}

	public function get_user_reservations($name){
		$this->db->select('r.id as id,r.seat as seat,s.time as time,m.name as movie,c.name as cinema,r.canceled');
		$this->db->from('reservations as r');
		$this->db->join('schedule as s', 'r.schedule_id = s.id');
		$this->db->join('cinema as c', 's.cinema_id = c.id');
		$this->db->join('movie as m', 's.movie_id = m.id');
		$this->db->where('r.name', $name);
		return $this->db->get()->result_array();
	}

	public function cancel_reservation($id){
		$this->db->where(array('id'=>$id));
		return $this->db->update('reservations', array('canceled' => 1));
	}

	public function request_cancel_reservation($id){
		$this->db->where(array('id'=>$id));
		return $this->db->update('reservations', array('canceled' => 2));
	}

	function create_reservation_by_name($name,$seat,$schedule){
		$data = array('name'=>$name,'seat'=>$seat,'schedule_id'=>$schedule);
		return $this->db->insert('reservations',$data);
	}

	public function get_reserved_seats($id){
		$this->db->select('seat');
		$this->db->from('reservations');
		$this->db->where(array('schedule_id' => $id));
		$this->db->where('canceled!=',1);
		return $this->db->get()->result_array();
	}


}