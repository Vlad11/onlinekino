<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movie_model extends CI_Model {


	public function insert_movie($name,$year,$country,$actors,$desc,$length,$genre,$director){
		$data = array('name'=>$name,'year'=>$year,'country'=>$country,'actors'=>$actors,'description'=>$desc,'length'=>$length,'genre'=>$genre,'director'=>$director);
		return $this->db->insert('movie',$data);
	}

	public function update_movie($id,$name,$year,$country,$actors,$desc,$length,$genre,$director){
		$this->db->where(array('id'=>$id));
		return $this->db->update('movie',array('name'=>$name,'year'=>$year,'country'=>$country,'actors'=>$actors,'description'=>$desc,'length'=>$length,'genre'=>$genre,'director'=>$director));
	}

	public function get_movies(){
		return $this->db->get('movie')->result_array();
	}

	public function remove_movie($id){
		$this->db->where('id',$id);
		return $this->db->delete('movie');
	}
}