<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cinema_model extends CI_Model {

	public function is_unique($name){
		return !count($this->db->get_where('users', array('name' => $name))->result_array());
	}

	public function insert_cinema($name,$seatsNo,$desc,$addr,$seats){
		$data = array('name'=>$name,'number_of_seats'=>$seatsNo,'description'=>$desc,'seats'=>$seats,'address'=>$addr);
		return $this->db->insert('cinema',$data);
	}

	public function update_cinema($id,$name,$seatsNo,$desc,$addr,$seats){
		$this->db->where(array('id'=>$id));
		return $this->db->update('cinema',array('name'=>$name,'number_of_seats'=>$seatsNo,'description'=>$desc,'seats'=>$seats,'address'=>$addr));
	}

	public function remove_cinema($id){
		$this->db->where('id',$id);
		return $this->db->delete('cinema');
	}

	public function get_cinemas(){
		return $this->db->get('cinema')->result_array();
	}
}