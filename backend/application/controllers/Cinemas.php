<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cinemas extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->model('cinema_model');
    }

    function get_cinemas(){
        print_r(json_encode($this->cinema_model->get_cinemas()));
    }

    function add_new_cinema(){
        $d = json_decode(trim(file_get_contents('php://input')), true);
        if($this->cinema_model->insert_cinema($d['name'],$d['numberOfSeats'],$d['description'],$d['address'],'')){
            echo 1;
        }
        else{
            echo 0;
        }
    }

    function remove_cinema(){
        $d = json_decode(trim(file_get_contents('php://input')), true);
        $this->cinema_model->remove_cinema($d['id']);
    }

    function update_cinema(){
        $d = json_decode(trim(file_get_contents('php://input')), true);
        if($this->cinema_model->update_cinema($d['id'],$d['name'],$d['numberOfSeats'],$d['description'],$d['address'],'')){
            echo 1;
        }
        else{
            echo 0;
        }
    }
}