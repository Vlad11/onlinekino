<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reservations extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->model('reservation_model');
    }

    function check_by_name(){
        $data = json_decode(trim(file_get_contents('php://input')), true);
        echo json_encode($this->reservation_model->getByName($data['name']));
    }

    function get_active_reservations(){
        echo json_encode($this->reservation_model->get_active_reservations());
    }

    function get_user_reservations(){
    	$d = json_decode(trim(file_get_contents('php://input')), true);
        echo json_encode($this->reservation_model->get_user_reservations($d['name']));
    }

    function cancel_reservation(){
    	$d = json_decode(trim(file_get_contents('php://input')), true);
        echo json_encode($this->reservation_model->cancel_reservation($d['id']));
    }

    function request_cancel_reservation(){
    	$d = json_decode(trim(file_get_contents('php://input')), true);
        echo json_encode($this->reservation_model->request_cancel_reservation($d['id']));
    }

    function create_reservation_by_name(){
    	$d = json_decode(trim(file_get_contents('php://input')), true);

    	if($this->reservation_model->valid_name($d['name'])){
    		if($this->reservation_model->is_name_unique($d['name'])){
    			$this->reservation_model->create_reservation_by_name($d['name'],$d['seat'],$d['id']);
    			echo 1;
    		}
    		else{
    			echo 0;
    		}
    	}
    	else{
    		echo 2; // meno je uz registrovane
    	}
    }

    function create_reservation_by_name_logged(){
    	$d = json_decode(trim(file_get_contents('php://input')), true);
    	if($this->reservation_model->create_reservation_by_name($d['name'],$d['seat'],$d['id'])){
    		echo 1;
    	}
    	else{
    		echo 0;
    	}
    }

    function get_reserved_seats(){
    	$d = json_decode(trim(file_get_contents('php://input')), true);
    	echo json_encode($this->reservation_model->get_reserved_seats($d['schedule']));
    }
}