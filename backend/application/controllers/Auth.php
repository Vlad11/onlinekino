<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct()
        {
                parent::__construct();
                $this->load->model('auth_model');
        }
	
	function index(){
		$this->load->view('register_view');
	}

	public function registration(){
                $data = json_decode(trim(file_get_contents('php://input')), true);
                if(ctype_alnum($data['name']) && (strlen($data['name']) >= 2) && (strlen($data['password']) >= 5)){
                        if($this->auth_model->is_unique($data['name'])){
                                if($this->auth_model->insert_new_user($data['name'],$data['password'])){
                                        echo 0;
                                }
                                else{
                                        echo 3;
                                }
                                
                        }
                        else{
                                echo 2;
                        }
                }
                else{   
                        echo 1;
                }
	}

	public function login(){
                $data = json_decode(trim(file_get_contents('php://input')), true);
                if(ctype_alnum($data['name']) && (strlen($data['name']) >= 2) && (strlen($data['password']) >= 5)){
                        if(!$this->auth_model->is_unique($data['name'])){
                               /* $ses_data['logged_in'] = true;
                                $ses_data['user_id'] = $this->auth_model->get_id($data['name']);
                                $this->session->set_userdata($ses_data);*/
                                echo 5;
                        }
                        else{
                                echo 4;
                        }
                }
                else{
                        echo 1;
                }
	}

        public function is_admin(){
                $data = json_decode(trim(file_get_contents('php://input')), true);
                if($this->auth_model->is_admin($data['name'])){
                        echo 1;
                }
                else{
                        echo 0;
                }
        }

}

?>