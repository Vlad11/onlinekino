<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movies extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->model('movie_model');
    }


    function add_new_movie(){
        $d = json_decode(trim(file_get_contents('php://input')), true);
        if($this->movie_model->insert_movie($d['name'],$d['year'],$d['country'],$d['actors'],$d['description'],$d['length'],$d['genre'],$d['director'])){
            echo 1;
        }
        else{
            echo 0;
        }
    }

    function get_movies(){
        print_r(json_encode($this->movie_model->get_movies()));
    }

    function remove_movie(){
        $d = json_decode(trim(file_get_contents('php://input')), true);
        $this->movie_model->remove_movie($d['id']);
    }

    function update_movie(){
        $d = json_decode(trim(file_get_contents('php://input')), true);
        if($this->movie_model->update_movie($d['id'],$d['name'],$d['year'],$d['country'],$d['actors'],$d['description'],$d['length'],$d['genre'],$d['director'])){
            echo 1;
        }
        else{
            echo 0;
        }
    }
}