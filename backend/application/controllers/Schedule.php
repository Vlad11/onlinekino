<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Schedule extends CI_Controller {
	function __construct(){
        parent::__construct();
        $this->load->model('schedule_model');
    }

    function remove_schedule(){
        $d = json_decode(trim(file_get_contents('php://input')), true);
        $this->schedule_model->remove_schedule($d['id']);
    }

    function get_schedules(){
        print_r(json_encode($this->schedule_model->get_schedules()));
    }

    function get_schedule_detail(){
        $d = json_decode(trim(file_get_contents('php://input')), true);
        print_r(json_encode($this->schedule_model->get_schedule_detail($d['id'])));
    }

    function add_schedule(){
        $d = json_decode(trim(file_get_contents('php://input')), true);
        if($this->schedule_model->add_schedule($d['cinema'],$d['movie'],$d['time'])){
            echo 1;
        }
        else{
            echo 0;
        }
    }
}